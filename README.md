# README

Analysis of Transect data for Valentine Cave in Lava Beds National Monument.

### .ipynb files
These are JSON respresentations of Jupyter Notebook files. Download and import these into Jupyter Notebook. You will need a Chart Studio account to generate your own graphs of the data, but I have added a link to each plot in the comments (hosted on my Chart Studio account). 

### Charts
The charts are separated into 2 major categories: full datasets and priority targets.
#### Full Datasets
Because of the amount of data, I've stacked each person's targets vertically so they don't intersect in the graph. The priority function of this chart is to visualize the horizontal distribution of the targets. Verticle distribution is within a 1.5-meter range.
#### Priority Targets
Each scientist was instructed to choose 5 targets that they believed to be more important than the other 15-20 targets they chose. These targets are graphed without being stacked, as the vertical distribution is important to take note of on a smaller scale.